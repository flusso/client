import React from "react";
import _ from 'lodash';
import {push} from 'connected-react-router'
import {withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {Paper, Tabs, Tab} from "@material-ui/core";
import MemoryTwoToneIcon from '@material-ui/icons/MemoryTwoTone';
import PeopleAltTwoToneIcon from '@material-ui/icons/PeopleAltTwoTone';
import AttachMoneyTwoToneIcon from '@material-ui/icons/AttachMoneyTwoTone';

const tabs = [
    {route: '/admin/runners', component: <Tab icon={<MemoryTwoToneIcon/>} label={'runners'}/>},
    {route: '/admin/variables', component: <Tab icon={<AttachMoneyTwoToneIcon/>} label={'variables'}/>},
    {route: '/admin/users', component: <Tab icon={<PeopleAltTwoToneIcon/>} label={'users'}/>}
]

function AdminTabsComponent({push, match}) {
    const routeIndex = _.findIndex(tabs, {route: match.url});
    const value = routeIndex !== -1 ? routeIndex : 0;

    return (
        <Paper square style={{marginBottom: 10}}>
            <Tabs indicatorColor="primary" textColor="primary" value={value} onChange={(_e, selectedTab) => push(tabs[selectedTab].route)}>
                {tabs.map(({component}) => component)}
            </Tabs>
        </Paper>
    );
}

export const AdminTabs = withRouter(connect(null, {push})(AdminTabsComponent));
