import {get, put} from 'api';
import _ from "lodash";

import {getUser} from "./selectors";

function setUser(user, isDirty = false) {
    return {type: 'SET_USER', user, isDirty};
}

export function fetchUser(userId) {
    return async dispatch => {
        const user = await get(`/users/${userId}`);

        dispatch(setUser(user));
    }
}

export function saveUser() {
    return async (dispatch, getState) => {
        const user = getUser(getState());

        if (!user?._id) {
            return null;
        }

        await put(`/users/${user._id}`, user);

        dispatch(fetchUser(user._id));
    }
}

export function updateUser(property, event) {
    return (dispatch, getState) => {
        const state = getState();
        let value = _.get(event, ['target', 'value'], event);
        const user = getUser(state);

        if (property === 'permissions') {
            if (value.indexOf(',') !== -1) {
                value = (value || '').split(', ');
            } else {
                value = [value];
            }
            value = value.map(el => el.trim())
        }

        _.set(user, property, value);

        dispatch(setUser({...user}, true));
    };
}