import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from "moment";
import {Grid, Paper, TextField, Checkbox, Button} from "@material-ui/core";
import SaveTwoToneIcon from "@material-ui/icons/Save";

import {BackButton} from 'components';
import {registerRoute} from "router";

import {fetchUser, saveUser, updateUser} from './actions';
import {getUser, getUserDirtyness} from './selectors';
import {getLastContactTime} from "components";

function EditableEntryComponent({text, value, property, label, disabled = false, updateUser}) {
    const valueType = typeof value;

    return (
        <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
            <h2>{text} </h2>
            {(valueType === 'string' || valueType === 'number') && <TextField
                variant={'outlined'}
                onChange={e => !disabled && updateUser(property, e.target.value)}
                value={value}
                label={label}
                disabled={disabled}
                style={{marginLeft: 20}}
            />}
            {valueType === 'boolean' && <Checkbox
                color={'primary'}
                onChange={e => !disabled && updateUser(property, e.target.checked)}
                checked={value}
                disabled={disabled}
                style={{marginLeft: 20}}
            />}
        </div>
    );
}

class UserComponent extends Component {
    componentDidMount() {
        this.props.fetchUser(this.props.match.params.userId);
    }

    render() {
        const {user, isDirty, saveUser} = this.props;

        if (!user) {
            return null;
        }

        const {email, permissions = [], active, username, created} = user;

        return (
            <Grid item={true} xs={11}>
                <BackButton/>
                <Paper elevation={3} style={{marginBottom: 10, padding: 10, justifyContent: 'center', display: 'flex', flexDirection: 'column'}}>
                    <EditableEntry text={'Active'} value={!!active} property={'active'}/>
                    <EditableEntry text={'Username'} value={username} property={'username'}/>
                    <EditableEntry text={'Email'} value={email} property={'email'}/>
                    <EditableEntry text={'Permissions (comma separated)'} value={permissions.join(', ')} property={'permissions'}/>
                    <EditableEntry text={'Created'} value={moment(created).format('L HH:mm:ss')} label={getLastContactTime(created)} disabled={true}/>
                    <div style={{display: 'flex', flexDirection: 'row', margin: 20}}>
                        <div style={{flex: 1}}/>
                        <Button
                            onClick={saveUser}
                            variant={'contained'}
                            color={'primary'}
                            disabled={!isDirty}
                            startIcon={<SaveTwoToneIcon/>}
                        >
                            Save User
                        </Button>
                    </div>
                </Paper>
            </Grid>
        );
    }
}

const User = connect(state => ({user: getUser(state), isDirty: getUserDirtyness(state)}), {fetchUser, saveUser})(UserComponent);
const EditableEntry = connect(null, {updateUser})(EditableEntryComponent);
registerRoute({path: '/users/:userId', component: User});
