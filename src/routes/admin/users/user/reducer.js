import {registerReducer} from 'store';

const user = {
    SET_USER(state, {user, isDirty}) { return {user, isDirty}; },
    initialState: []
};

registerReducer('user', user);
