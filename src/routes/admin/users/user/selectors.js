export function getUser(state) {
    return state?.user?.user;
}

export function getUserDirtyness(state) {
    return state?.user?.isDirty;
}
