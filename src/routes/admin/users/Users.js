import React, {Component} from 'react';
import {connect} from 'react-redux';
import {push} from "connected-react-router";
import {TableRow, TableCell, Grid} from "@material-ui/core";

import {registerRoute} from "router";
import {Table} from "components";

import {fetchUsers} from './actions';
import {getUsers} from './selectors';
import {AdminTabs} from "../AdminTabs";

function UserRowComponent({user, push}) {
    return (
        <TableRow onClick={() => push(`/users/${user._id}`)}>
            <TableCell>{user.username}</TableCell>
            <TableCell>{user.email}</TableCell>
            <TableCell>{user.created}</TableCell>
            <TableCell>{user.permissions.join('')}</TableCell>
            <TableCell>{user.active.toString()}</TableCell>
        </TableRow>
    );
}

class UsersComponent extends Component {
    componentDidMount() {
        this.props.fetchUsers();
    }

    render() {
        const {users} = this.props;
        if (!users) {
            return null;
        }

        return (
            <Grid xs={11} spacing={2}>
                <AdminTabs/>
                <Grid xs={12}>
                    <Table
                        title={'Users'}
                        headers={['Username', 'Email', 'Created', 'Permissions', 'Active']}
                        rows={users.map((user, idx) => <UserRow user={user} key={idx}/>)}
                    />
                </Grid>
            </Grid>
        );
    }
}

const Users = connect(state => ({users: getUsers(state)}), {fetchUsers})(UsersComponent);
const UserRow = connect(null, {push})(UserRowComponent);
registerRoute({path: '/admin/users', component: Users});
