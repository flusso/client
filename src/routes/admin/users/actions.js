import {get} from 'api';

function setUsers(users) {
    return {type: 'SET_USERS', users};
}

export function fetchUsers() {
    return async dispatch => {
        const users = await get('/users');

        dispatch(setUsers(users));
    }
}
