import {registerReducer} from 'store';

const users = {
    SET_USERS(state, {users}) { return users; },
    initialState: []
};

registerReducer('users', users);
