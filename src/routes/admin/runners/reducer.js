import {registerReducer} from 'store';

const runners = {
    SET_RUNNERS(state, {runners}) { return runners; },
    initialState: []
};

registerReducer('runners', runners);
