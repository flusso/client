import {registerReducer} from 'store';

const runner = {
    SET_RUNNER(state, {runner, isDirty}) { return {runner, isDirty}; },
    initialState: []
};

registerReducer('runner', runner);
