import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from "moment";
import {Grid, Paper, TextField, Checkbox, Button} from "@material-ui/core";
import SaveTwoToneIcon from "@material-ui/icons/Save";

import {BackButton} from 'components';
import {registerRoute} from "router";

import {fetchRunner, saveRunner, updateRunner} from './actions';
import {getRunner, getRunnerDirtyness} from './selectors';
import {getLastContactTime} from "components";

function EditableEntryComponent({text, value, property, label, disabled = false, updateRunner}) {
    const valueType = typeof value;

    return (
        <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
            <h2>{text} </h2>
            {(valueType === 'string' || valueType === 'number') && <TextField
                variant={'outlined'}
                onChange={e => !disabled && updateRunner(property, e.target.value)}
                value={value}
                label={label}
                disabled={disabled}
                style={{marginLeft: 20}}
            />}
            {valueType === 'boolean' && <Checkbox
                color={'primary'}
                onChange={e => !disabled && updateRunner(property, e.target.checked)}
                checked={value}
                disabled={disabled}
                style={{marginLeft: 20}}
            />}
        </div>
    );
}

class RunnerComponent extends Component {
    componentDidMount() {
        this.props.fetchRunner(this.props.match.params.runnerId);
    }

    render() {
        const {runner, isDirty, saveRunner} = this.props;

        if (!runner) {
            return null;
        }

        const {name, tags = [], active, ip, created, lastContact, maxConcurrentJobs} = runner;

        return (
            <Grid item={true} xs={11}>
                <BackButton/>
                <Paper elevation={3} style={{marginBottom: 10, padding: 10, justifyContent: 'center', display: 'flex', flexDirection: 'column'}}>
                    <EditableEntry text={'Active'} value={!!active} property={'active'}/>
                    <EditableEntry text={'Name'} value={name} disabled={true}/>
                    <EditableEntry text={'Tags (comma separated)'} value={tags.join(', ')} property={'tags'}/>
                    <EditableEntry text={'Ip'} value={ip} disabled={true}/>
                    <EditableEntry text={'Concurrent jobs'} value={maxConcurrentJobs} property={'maxConcurrentJobs'}/>
                    <EditableEntry text={'Created'} value={moment(created).format('L HH:mm:ss')} label={getLastContactTime(created)} disabled={true}/>
                    <EditableEntry text={'Last contact'} value={moment(lastContact).format('L HH:mm:ss')} label={getLastContactTime(lastContact)} disabled={true}/>
                    <div style={{display: 'flex', flexDirection: 'row', margin: 20}}>
                        <div style={{flex: 1}}/>
                        <Button
                            onClick={saveRunner}
                            variant={'contained'}
                            color={'primary'}
                            disabled={!isDirty}
                            startIcon={<SaveTwoToneIcon/>}
                        >
                            Save Runner
                        </Button>
                    </div>
                </Paper>
            </Grid>
        );
    }
}

const Runner = connect(state => ({runner: getRunner(state), isDirty: getRunnerDirtyness(state)}), {fetchRunner, saveRunner})(RunnerComponent);
const EditableEntry = connect(null, {updateRunner})(EditableEntryComponent);
registerRoute({path: '/runners/:runnerId', component: Runner});
