export function getRunner(state) {
    return state?.runner?.runner;
}

export function getRunnerDirtyness(state) {
    return state?.runner?.isDirty;
}
