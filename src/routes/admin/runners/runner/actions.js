import {get, put} from 'api';
import _ from "lodash";

import {getRunner} from "./selectors";

function setRunner(runner, isDirty = false) {
    return {type: 'SET_RUNNER', runner, isDirty};
}

export function fetchRunner(runnerId) {
    return async dispatch => {
        const runner = await get(`/runners/${runnerId}`);

        dispatch(setRunner(runner));
    }
}

export function saveRunner() {
    return async (dispatch, getState) => {
        const runner = getRunner(getState());

        if (!runner?._id) {
            return null;
        }

        await put(`/runners/${runner._id}`, runner);

        dispatch(fetchRunner(runner._id));
    }
}

export function updateRunner(property, event) {
    return (dispatch, getState) => {
        const state = getState();
        let value = _.get(event, ['target', 'value'], event);
        const runner = getRunner(state);

        if (property === 'tags') {
            if (value.indexOf(',') !== -1) {
                value = (value || '').split(', ');
            } else {
                value = [value];
            }
            value = value.map(el => el.trim())
        }

        _.set(runner, property, value);

        dispatch(setRunner({...runner}, true));
    };
}