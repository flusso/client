import {get} from 'api';

function setRunners(runners) {
    return {type: 'SET_RUNNERS', runners};
}

export function fetchRunners() {
    return async dispatch => {
        const runners = await get('/runners');

        dispatch(setRunners(runners));
    }
}
