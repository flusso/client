import React, {Component} from 'react';
import {connect} from 'react-redux';
import {push} from "connected-react-router";
import {TableRow, TableCell, Button, Grid} from "@material-ui/core";

import AddCircleTwoToneIcon from '@material-ui/icons/AddCircleTwoTone';

import {registerRoute} from "router";
import {getLastContactTime, showDialog, Table} from "components";

import {fetchRunners} from './actions';
import {getRunners} from './selectors';
import {AddRunnerDialog} from "./AddRunnerDialog";
import {AdminTabs} from "../AdminTabs";

function RunnerRowComponent({runner, push}) {
    const lastContact = getLastContactTime(runner.lastContact);

    return (
        <TableRow onClick={() => push(`/runners/${runner._id}`)}>
            <TableCell>{runner.name}</TableCell>
            <TableCell>{runner.ip}</TableCell>
            <TableCell>{lastContact}</TableCell>
            <TableCell>{runner.tags.join('')}</TableCell>
            <TableCell>{runner.active.toString()}</TableCell>
        </TableRow>
    );
}

class RunnersComponent extends Component {
    componentDidMount() {
        this.props.fetchRunners();
    }

    render() {
        const {runners, showDialog} = this.props;
        if (!runners) {
            return null;
        }

        return (
            <Grid xs={11} spacing={2}>
                <AdminTabs/>
                <Grid xs={12}>
                    <Table
                        title={'Runners'}
                        rightHeader={
                            <Button onClick={() => showDialog({content: () => <AddRunnerDialog/>})} variant={"contained"} color={"primary"} startIcon={<AddCircleTwoToneIcon/>}>Add Runner</Button>
                        }
                        headers={['Name', 'IP', 'Last contact', 'Tags', 'Active']}
                        rows={runners.map((runner, idx) => <RunnerRow runner={runner} key={idx}/>)}
                    />
                </Grid>
            </Grid>
        );
    }
}

const Runners = connect(state => ({runners: getRunners(state)}), {fetchRunners, showDialog})(RunnersComponent);
const RunnerRow = connect(null, {push})(RunnerRowComponent);
registerRoute({path: '/admin/runners', component: Runners});
