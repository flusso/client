import React, {Component} from 'react';
import {List, ListItem, ListItemText, Divider, Tooltip} from '@material-ui/core';
import FileCopyTwoToneIcon from '@material-ui/icons/FileCopyTwoTone';

import {get} from 'api';

function CopyableCode({text}) {
    return (
        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
            <code>{text}</code>
            <Tooltip title={'Copy'} placement={'right'}>
                <FileCopyTwoToneIcon style={{marginLeft: 20}} onClick={() => navigator.clipboard.writeText(text)}/>
            </Tooltip>
        </div>
    )
}

function getSteps({token}) {
    return [
        <ListItem style={{display: 'flex', flexDirection: 'column'}}>
            <ListItemText primary={'Install the runner'}/>
            <CopyableCode text={'curl -sSL https://runner.flus.so | bash'}/>
        </ListItem>,
        <ListItem style={{display: 'flex', flexDirection: 'column'}}>
            <ListItemText primary={'Register the runner'}/>
            <CopyableCode text={'yarn register'}/>
        </ListItem>,
        <ListItem style={{display: 'flex', flexDirection: 'column'}}>
            <ListItemText primary={'The `runnerRegistrationToken` is'}/>
            <CopyableCode text={token || ''}/>
        </ListItem>,
        <ListItem>
            <ListItemText primary={'If everything went fine, you should see the new runner in the list!'}/>
        </ListItem>
    ];
}

export class AddRunnerDialog extends Component {
    state = {token: {}};

    async componentDidMount() {
        try {
            const token = await get('/runnerRegistrationToken');

            this.setState({token});
        } catch (e) {}
    }

    render() {
        return (
            <List>
                {getSteps(this.state.token).map((Step, index) => {
                    return (
                        <div key={index}>
                            {Step}
                            <Divider/>
                        </div>
                    );
                })}
            </List>
        );
    }
}