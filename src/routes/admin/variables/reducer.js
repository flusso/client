import {registerReducer} from 'store';

const variables = {
    SET_VARIABLES(state, {variables}) { return variables; },
    initialState: []
};

registerReducer('variables', variables);
