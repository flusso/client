import React, {Component} from 'react';
import {connect} from 'react-redux';
import {TableRow, TableCell, Grid, Button, TextField, IconButton} from "@material-ui/core";
import AddCircleTwoToneIcon from "@material-ui/icons/AddCircleTwoTone";
import SaveTwoToneIcon from '@material-ui/icons/SaveTwoTone';
import DeleteForeverTwoToneIcon from "@material-ui/icons/DeleteForeverTwoTone";

import {registerRoute} from "router";
import {Table} from "components";

import {addNewVariable, fetchVariables, createVariable, deleteVariable} from './actions';
import {getVariables} from './selectors';
import {AdminTabs} from "../AdminTabs";
class VariableRowComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {name: props.variable.name, value: props.variable.value};
    }

    render() {
        const {createVariable, deleteVariable, variable} = this.props;
        const {name, value} = this.state;

        return (
            <TableRow>
                <TableCell>
                    <TextField
                        variant={'outlined'}
                        size={'small'}
                        onChange={e => this.setState({name: e.target.value})}
                        value={name}
                        style={{marginLeft: 20}}
                    />
                </TableCell>
                <TableCell>
                    <TextField
                        variant={'outlined'}
                        size={'small'}
                        onChange={e => this.setState({value: e.target.value})}
                        value={value}
                        style={{marginLeft: 20}}
                    />
                </TableCell>
                <TableCell>
                    <Button style={{marginRight: 10}} onClick={() =>createVariable({name, value})} variant={"outlined"} color={"primary"} startIcon={<SaveTwoToneIcon/>}>Save</Button>
                    {!!variable._id && <Button
                        onClick={() => deleteVariable(variable._id)}
                        variant={"outlined"}
                        color={"secondary"}
                        startIcon={<DeleteForeverTwoToneIcon/>}>
                        Delete
                    </Button>}
                </TableCell>
            </TableRow>
        );
    }
}

class VariablesComponent extends Component {
    componentDidMount() {
        this.props.fetchVariables();
    }

    render() {
        const {variables, addNewVariable} = this.props;

        return (
            <Grid xs={11} spacing={2}>
                <AdminTabs/>
                <Grid xs={12}>
                    <Table
                        title={'Variables'}
                        rightHeader={
                            <Button onClick={addNewVariable} variant={"contained"} color={"primary"} startIcon={<AddCircleTwoToneIcon/>}>New Variable</Button>
                        }
                        headers={['Name', 'Value', 'Actions']}
                        rows={variables.map((variable, idx) => <VariableRow variable={variable} key={idx}/>)}
                    />
                </Grid>
            </Grid>
        );
    }
}

const Variables = connect(state => ({variables: getVariables(state)}), {addNewVariable, fetchVariables})(VariablesComponent);
const VariableRow = connect(null, {createVariable, deleteVariable})(VariableRowComponent);
registerRoute({path: '/admin/variables', component: Variables});
