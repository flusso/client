import {del, get, post} from 'api';
import {getVariables} from "./selectors";

function setVariables(variables) {
    return {type: 'SET_VARIABLES', variables};
}

export function addNewVariable() {
    return (dispatch, getState) => {
        const variables = getVariables(getState());

        dispatch(setVariables([...variables, {name: 'New variable', value: ''}]));
    }
}

export function fetchVariables() {
    return async dispatch => {
        const variables = await get('/variables');

        dispatch(setVariables(variables));
    }
}

export function createVariable(variable) {
    return async dispatch => {
        await post('/variables', variable);

        dispatch(fetchVariables());
    }
}

export function deleteVariable(_id) {
    return async dispatch => {
        await del(`/variables/${_id}`);

        dispatch(fetchVariables());
    }
}