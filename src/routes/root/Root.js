import {push} from 'connected-react-router';
import {connect} from "react-redux";
import {registerRoute} from "router";

function RootComponent({push}) {
    push('/flows');

    return null;
}

const Root = connect(null, {push})(RootComponent);

registerRoute({path: '/', component: Root});