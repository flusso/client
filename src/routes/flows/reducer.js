import {registerReducer} from 'store';

const flows = {
    SET_FLOWS(state, {flows = []}) { return flows; },
};

registerReducer('flows', flows);
