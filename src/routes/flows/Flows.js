import React, {Component} from 'react';
import {connect} from "react-redux";
import moment from "moment";
import _ from "lodash";
import {push} from 'connected-react-router'
import {TableRow, TableCell, Button, Grid} from '@material-ui/core';
import AddCircleTwoToneIcon from '@material-ui/icons/AddCircleTwoTone';
import PlayCircleFilledTwoToneIcon from '@material-ui/icons/PlayCircleFilledTwoTone';
import CheckTwoToneIcon from '@material-ui/icons/CheckTwoTone';
import CloseTwoToneIcon from '@material-ui/icons/CloseTwoTone';
import NotInterestedTwoToneIcon from '@material-ui/icons/NotInterestedTwoTone';

import {registerRoute} from "router";
import {Table} from "components";

import {getFlows} from "./selectors";
import {createPipeline, fetchFlows} from "./actions";

function FlowRowComponent({flow, push, createPipeline}) {
    return (
        <TableRow onClick={() => push(`/flows/${flow._id}`)}>
            {flow.active ?
                <TableCell><PlayCircleFilledTwoToneIcon fontSize={'large'} onClick={() => createPipeline(flow._id)}/></TableCell> :
                <TableCell><NotInterestedTwoToneIcon fontSize={'large'}/></TableCell>
            }
            <TableCell>{flow.name}</TableCell>
            <TableCell>{flow.active ? <CheckTwoToneIcon/> : <CloseTwoToneIcon/> }</TableCell>
            <TableCell>{flow.runningPipelines || 0}</TableCell>
            <TableCell>{flow.pendingPipelines || 0}</TableCell>
            <TableCell>{moment(flow.created).format('L HH:mm:ss')}</TableCell>
            <TableCell>{moment(flow.modified).format('L HH:mm:ss')}</TableCell>
        </TableRow>
    );
}

const FlowRow = connect(null, {createPipeline, push})(FlowRowComponent);

class FlowsComponent extends Component {
    componentDidMount() {
        this.props.fetchFlows();
    }

    render() {
        let {flows, push} = this.props;

        if (_.isEmpty(flows)) {
            flows = [];
        }

        return (
            <Grid xs={11}>
                <Table
                    title={'Flows'}
                    rightHeader={
                        <Button onClick={() => push('/flows/new')} variant={"contained"} color={"primary"} startIcon={<AddCircleTwoToneIcon/>}>New Flow</Button>
                    }
                    headers={['Start', 'Name', 'Active', 'Running Pipelines', 'Pending Pipelines', 'Created', 'Modified']}
                    rows={flows.map((flow, idx) => <FlowRow flow={flow} key={idx}/>)}
                />
            </Grid>
        );
    }
}

const Flows = connect(state => ({flows: getFlows(state)}), {fetchFlows, push})(FlowsComponent);
registerRoute({path: '/flows', component: Flows});
