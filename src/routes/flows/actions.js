import {push} from 'connected-react-router'

import {get, post} from 'api';

function setFlows(flows) {
    return {type: 'SET_FLOWS', flows};
}

export function fetchFlows() {
    return async dispatch => {
        const flows = await get('/flows');

        dispatch(setFlows(flows));
    }
}

export function createPipeline(flowId) {
    return async dispatch => {
        const pipeline = await post(`/pipelines/${flowId}`);

        if (pipeline && pipeline._id) {
            dispatch(push(`/pipelines/${pipeline._id}`))
        } else {
            dispatch(fetchFlows());
        }
    }
}