import {post} from 'api';
import {login} from '../login/actions';

export function signup(email, username, password, repeatPassword) {
    return async dispatch => {
        const result = await post('/signup', {email, username, password, repeatPassword});

        if (result) {
            dispatch(login(email, password));
        }
    }
}
