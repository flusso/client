import React, {Component} from 'react';
import {connect} from 'react-redux';

import {registerRoute} from "router";
import {signup} from "./actions";
import {getToken} from "./selectors";
import {Paper} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

class SignupComponent extends Component {
    state = {email: '', username: '', password: '', repeatPassword: ''};

    arePasswordInvalid() {
        const {password, repeatPassword} = this.state;
        const areMatching = password && repeatPassword && password === repeatPassword;
        const longEnough = password.length >= 8;

        if (!areMatching) {
            return 'The passwords are not matching!';
        }

        if (!longEnough) {
            return 'At least 8 characters please!';
        }


        return false;
    }

    isEmailInvalid() {
        const {email} = this.state;

        const isValid = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email);

        if (!isValid) {
            return 'Invalid email format!';
        }

        return false;
    }

    isUsernameInvalid() {
        const {username} = this.state;

        const isValid = username.length >= 3;

        if (!isValid) {
            return 'At least 3 characters please!';
        }

        return false;
    }

    render() {
        const {signup} = this.props;
        const {email, username, password, repeatPassword} = this.state;

        const passwordErrorMessage = this.arePasswordInvalid();
        const emailErrorMessage = this.isEmailInvalid();
        const usernameErrorMessage = this.isUsernameInvalid();
        const cannotSubmit = passwordErrorMessage || emailErrorMessage || usernameErrorMessage;

        return (
            <Grid item={true} xs={11} style={{justifyContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column'}}>
                <Paper elevation={5} style={{padding: 50, flexDirection: 'column', display: 'flex', zIndex: 1}}>
                    <h1 style={{textAlign: 'center'}}>Signup</h1>
                    <TextField
                        margin={'normal'}
                        error={username && usernameErrorMessage}
                        helperText={username && usernameErrorMessage}
                        label="Username"
                        variant="outlined"
                        value={username}
                        onChange={e => this.setState({username: e.target.value})}
                    />
                    <TextField
                        margin={'normal'}
                        error={email && emailErrorMessage}
                        helperText={email && emailErrorMessage}
                        label="Email"
                        variant="outlined"
                        value={email}
                        onChange={e => this.setState({email: e.target.value})}
                    />
                    <TextField
                        error={password && repeatPassword && passwordErrorMessage}
                        type={'password'}
                        margin={'normal'}
                        label="Password"
                        variant="outlined"
                        value={password} onChange={e => this.setState({password: e.target.value})}
                    />
                    <TextField
                        error={repeatPassword && passwordErrorMessage}
                        helperText={repeatPassword && passwordErrorMessage}
                        type={'password'}
                        margin={'normal'}
                        label="Password... again..."
                        variant="outlined"
                        value={repeatPassword} onChange={e => this.setState({repeatPassword: e.target.value})}
                    />
                    <Button
                        disabled={cannotSubmit}
                        style={{marginTop: 40}}
                        variant={'outlined'}
                        size={'large'}
                        color={'primary'}
                        onClick={() => signup(email, username, password, repeatPassword)}
                    >
                        Submit
                    </Button>
                </Paper>
            </Grid>
        );
    }
}

const Signup = connect(state => ({token: getToken(state)}), {signup})(SignupComponent);
registerRoute({path: '/signup', component: Signup, auth: false});
