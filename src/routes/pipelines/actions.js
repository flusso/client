import {get} from 'api';

function setPipelines(pipelines) {
    return {type: 'SET_PIPELINES', pipelines};
}

export function fetchPipelines() {
    return async dispatch => {
        const pipelines = await get('/pipelines');

        dispatch(setPipelines(pipelines));
    }
}
