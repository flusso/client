import React, {Component} from 'react';
import {connect} from "react-redux";
import _ from 'lodash';
import {push} from "connected-react-router";
import {TableRow, TableCell, Grid} from "@material-ui/core";
import moment from "moment";

import {registerRoute} from "router";
import {Table, getStateIcon} from "components";

import {getPipelines} from "./selectors";
import {fetchPipelines} from "./actions";

function PipelineRowComponent({pipeline, push}) {
    return (
        <TableRow onClick={() => push(`/pipelines/${pipeline._id}`)}>
            <TableCell>{pipeline.flowName}</TableCell>
            <TableCell style={{display: 'flex', alignItems: 'center'}}>{getStateIcon(pipeline)}{pipeline.state}</TableCell>
            <TableCell>{moment(pipeline.started).format('L HH:mm:ss')}</TableCell>
            <TableCell>{moment(pipeline.lastExecution).format('L HH:mm:ss')}</TableCell>
        </TableRow>
    );
}

const PipelineRow = connect(null, {push})(PipelineRowComponent);

class PipelinesComponent extends Component {
    componentDidMount() {
        this.props.fetchPipelines();
    }

    render() {
        const {pipelines} = this.props;
        if (_.isEmpty(pipelines)) {
            return null;
        }

        return (
            <Grid xs={11}>
                <Table
                    title={'Pipelines'}
                    headers={['Flow', 'State', 'Created', 'Last Execution']}
                    rows={pipelines.map((pipeline, idx) => <PipelineRow pipeline={pipeline} key={idx}/>)}
                />
            </Grid>
        );
    }
}

const Pipelines = connect(state => ({pipelines: getPipelines(state)}), {fetchPipelines})(PipelinesComponent);
registerRoute({path: '/pipelines', component: Pipelines});
