import {registerReducer} from 'store';

const pipelines = {
    SET_PIPELINES(state, {pipelines}) { return pipelines; },
};
const pipelineJobs = {
    SET_PIPELINE_JOBS(state, {pipelineJobs}) { return pipelineJobs; },
};

registerReducer('pipelineJobs', pipelineJobs);
registerReducer('pipelines', pipelines);
