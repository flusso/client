import {get} from 'api';
import _ from "lodash";

import {getPipelineJobs} from "./selectors";

export function setPipelineJobs(pipelineJobs) {
    return {type: 'SET_PIPELINE_JOBS', pipelineJobs};
}

export function fetchPipelineJobs(pipelineId) {
    return async dispatch => {
        const jobs = await get('/jobs/', {pipelineId});

        dispatch(setPipelineJobs(jobs));
    }
}

export function updatePipelineJob(property, event, jobId) {
    return async (dispatch, getState) => {
        const state = getState();
        let value = _.get(event, ['target', 'value'], event);
        let pipelineJobs = getPipelineJobs(state);
        const jobIndex = _.findIndex(pipelineJobs, {_id: jobId});

        if (jobIndex === -1) { return; }

        if (Array.isArray(value)) {
            _.set(pipelineJobs[jobIndex], property, [..._.get(pipelineJobs, [jobIndex, property], []), ...value]);
        } else {
            _.set(pipelineJobs[jobIndex], property, value);
        }

        dispatch(setPipelineJobs([...pipelineJobs]));
    }
}
