import React, {Component} from 'react';
import {connect} from "react-redux";
import _ from 'lodash';
import {Grid} from "@material-ui/core";

import {BackButton} from "components";
import {registerRoute} from "router";
import {newWebsocket, sendWs} from "websockets";

import {getPipelineJobs} from "./selectors";
import {fetchPipelineJobs, updatePipelineJob} from "./actions";
import {Job} from "./job";
import {setFlow} from "../flow/actions";
import {Logs} from "./job/Logs";
import {PipelineChart} from "./PipelineChart";

class PipelinesComponent extends Component {
    constructor(props) {
        super(props);

        const {match, fetchPipelineJobs, updatePipelineJob} = props;

        this.ws = newWebsocket({
            onopen: () => sendWs(this.ws, {entityId: match.params.pipelineId}),
            onmessage: event => {
                const data = JSON.parse(event.data);
                const {action} = data;
                const actionsToRefresh = ['jobStarted', 'jobRetried', 'pipelineFinished'];

                if (_.includes(actionsToRefresh, action)) {
                    fetchPipelineJobs(match.params.pipelineId);
                } else if (action === 'logsReceived') {
                    updatePipelineJob('logs', data.logs, data.jobId);
                }
            }
        });
    }

    componentDidMount() {
        const {match, fetchPipelineJobs} = this.props;

        fetchPipelineJobs(match.params.pipelineId);
    }

    componentWillUnmount() {
        this.ws.close();
        this.props.setFlow({}, false);
    }

    render() {
        const {jobs, match} = this.props;
        if (_.isEmpty(jobs)) {
            return null;
        }

        return (
            <div style={{flex: 1}}>
                <BackButton/>
                <Grid container={true} style={{flex: 1}}>
                    <Grid xs={6} spacing={2}>
                        <Job/>
                        <PipelineChart pipelineId={match.params.pipelineId}/>
                    </Grid>
                    <Grid xs={6}>
                        <Logs/>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const Pipeline = connect(state => ({jobs: getPipelineJobs(state)}), {fetchPipelineJobs, setFlow, updatePipelineJob})(PipelinesComponent);
registerRoute({path: '/pipelines/:pipelineId', component: Pipeline});
