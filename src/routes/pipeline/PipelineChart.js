import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {Chart} from "components/Chart/Chart";
import {getNodes, addReExecutions, handleClick} from "components/Chart/actions";

import {getFirstJobNames} from "../flow/utils";
import {getPipelineJobs} from "./selectors";
import {setSelectedJobId} from "./job/actions";
import {getSelectedJobId} from "./job/selectors";
import {setPipelineJobs} from "./actions";

function getFirstJobId(jobs) {
    const firstJobName = _.first(getFirstJobNames(jobs));
    const firstJob = _.find(jobs, {name: firstJobName})

    return firstJob?._id;
}

class PipelineChartComponent extends Component {
    componentDidMount() {
        const {pipelineJobs, selectedJobId, setSelectedJobId} = this.props;

        if (!selectedJobId || !_.find(pipelineJobs, {_id: selectedJobId})) {
            const firstJobId = getFirstJobId(pipelineJobs);
            firstJobId && setSelectedJobId(firstJobId);
        }
    }

    componentWillUnmount() {
        this.props.setSelectedJobId();
        this.props.setPipelineJobs([]);
    }

    render() {
        const {pipelineJobs, selectedJobId, setSelectedJobId, handleClick} = this.props;
        const selectedJob = selectedJobId || getFirstJobId(pipelineJobs);

        let nodes = getNodes({
            totalJobs: pipelineJobs,
            selectedJob,
            setSelectedJob: setSelectedJobId,
            handleClick,
            isPipeline: true
        });

        nodes = addReExecutions({nodes, totalJobs: pipelineJobs, selectedJob, handleClick});

        return <Chart nodes={nodes}/>;
    }
}

export const PipelineChart = connect(state => ({
    pipelineJobs: getPipelineJobs(state),
    selectedJobId: getSelectedJobId(state),
}), {setSelectedJobId, setPipelineJobs, handleClick})(PipelineChartComponent);
