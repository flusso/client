import React from 'react';
import {connect} from "react-redux";
import _ from 'lodash';
import moment from 'moment';
import {Grid, Paper} from "@material-ui/core";
import WatchLaterTwoToneIcon from '@material-ui/icons/WatchLaterTwoTone';
import MemoryTwoToneIcon from '@material-ui/icons/MemoryTwoTone';
import InfoTwoToneIcon from '@material-ui/icons/InfoTwoTone';

import {getJob} from "./selectors";
import {Buttons} from "./Buttons";

function TopBarComponent({name, runnerName, tags}) {
    return (
        <Grid item={true} xs={12}>
            <Paper elevation={3} style={{padding: 20, display: 'flex', justifyContent: 'space-between'}}>
                <div style={{flexDirection: 'row'}}>
                    <span style={{display: 'flex', alignItems: 'center'}}>
                        <InfoTwoToneIcon fontSize={'large'} style={{marginRight: 20}}/>
                        <span>
                            <h2>Name</h2>
                            <h3>{name}</h3>
                        </span>
                    </span>
                    {runnerName && <span style={{display: 'flex', alignItems: 'center'}}>
                        <MemoryTwoToneIcon fontSize={'large'} style={{marginRight: 20}}/>
                        <span>
                            <h2>Running on runner</h2>
                            <h3>{runnerName}</h3>
                        </span>
                    </span>}
                    {!!tags[0] && <span>
                        <h2>Tags</h2>
                        <h3>{tags.join(', ')}</h3>
                    </span>}
                </div>
                <Times/>
            </Paper>
        </Grid>
    );
}

function TimesComponent({job}) {
    return (
        <div style={{flexDirection: 'row', display: 'flex'}}>
            <div style={{flexDirection: 'column'}}>
                <span style={{display: 'flex', flexDirection: 'column'}}>
                    <h2>Started</h2>
                    <span>{moment(job.started).format('L HH:mm:ss')}</span>
                </span>
                {job.finished && <span style={{display: 'flex', flexDirection: 'column'}}>
                    <h2>Finished</h2>
                    <span>{moment(job.finished).format('L HH:mm:ss')}</span>
                </span>}
            </div>
            <span style={{display: 'flex', alignItems: 'center'}}>
                <WatchLaterTwoToneIcon fontSize={'large'} style={{marginRight: 20}}/>
                <span>
                    <h2>Duration</h2>
                    <span>{moment.utc((job.finished ? moment(job.finished) : moment()).diff(moment(job.started))).format('HH:mm:ss')}</span>
                </span>
            </span>
        </div>
    );
}

function JobComponent({jobId}) {
    if (_.isEmpty(jobId)) {
        return null;
    }

    return (
        <Grid item={true} xs={12}>
            <Grid container={true} spacing={2}>
                <Buttons/>
            </Grid>
            <Grid container={true} spacing={2}>
                <TopBar/>
            </Grid>
        </Grid>
    );
}

export const Job = connect(state => ({jobId: getJob(state)?._id}))(JobComponent);
const TopBar = connect(state => ({
    name: getJob(state)?.name,
    runnerName: getJob(state)?.runnerName,
    tags: getJob(state)?.tags
}))(TopBarComponent);
const Times = connect(state => ({job: getJob(state)}))(TimesComponent);
