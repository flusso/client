import {put} from 'api';
import {getJob} from "./selectors";

export function startJob() {
    return (dispatch, getState) => {
        const job = getJob(getState());

        return put(`/jobs/${job._id}/start`);
    };
}

export function cancelJob() {
    return (dispatch, getState) => {
        const job = getJob(getState());

        return put(`/jobs/${job._id}/cancel`);
    };
}

export function retryJob() {
    return async (dispatch, getState) => {
        const job = getJob(getState());

        const newJob = await put(`/jobs/${job._id}/retry`);

        dispatch(setSelectedJobId(newJob._id));
    };
}

export function setSelectedJobId(selectedJobId) {
    return {type: 'SET_SELECTED_JOB_ID', selectedJobId};
}
