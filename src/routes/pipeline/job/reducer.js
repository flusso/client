import {registerReducer} from 'store';

const job = {
    SET_JOB(state, {job}) { return {...state, job}; },
    SET_SELECTED_JOB_ID(state, {selectedJobId}) { return {...state, selectedJobId}; },
};

registerReducer('job', job);
