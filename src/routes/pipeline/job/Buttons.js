import React from 'react';
import {connect} from "react-redux";
import {Grid, Paper, Button} from "@material-ui/core";
import TouchAppTwoToneIcon from '@material-ui/icons/TouchAppTwoTone';
import CancelTwoToneIcon from "@material-ui/icons/CancelTwoTone";
import ReplayTwoToneIcon from '@material-ui/icons/ReplayTwoTone';

import {getJob} from "./selectors";
import {startJob, cancelJob, retryJob} from "./actions";

function ButtonsComponent({jobState, jobExecution, startJob, cancelJob, retryJob}) {
    const canTriggerJob = jobState === 'created' && jobExecution === 'manual';
    const canCancelJob = jobState === 'pending' || jobState === 'running';
    const canRetryJob = jobState !== 'created' && jobState !== 'running' && jobState !== 'pending';

    if (!(canTriggerJob || canCancelJob || canRetryJob)) { return null; }

    return (
        <Grid item={true} xs={12}>
            <Paper elevation={3} style={{padding: 20, display: 'flex', justifyContent: 'space-between'}}>
                {canTriggerJob &&
                    <Button
                        onClick={startJob}
                        variant={'contained'}
                        startIcon={<TouchAppTwoToneIcon/>}
                        color={'primary'}>
                        Trigger Job
                    </Button>
                }
                {canCancelJob &&
                    <Button
                        onClick={cancelJob}
                        variant={'contained'}
                        startIcon={<CancelTwoToneIcon/>}
                        color={'secondary'}>
                        Cancel Job
                    </Button>
                }
                {canRetryJob &&
                    <Button
                        onClick={retryJob}
                        variant={'contained'}
                        startIcon={<ReplayTwoToneIcon/>}
                        color={'primary'}>
                        Retry Job
                    </Button>
                }
            </Paper>
        </Grid>
    );
}

export const Buttons = connect(state => ({
    jobState: getJob(state)?.state,
    jobExecution: getJob(state)?.execution,
}), {startJob, cancelJob, retryJob})(ButtonsComponent);
