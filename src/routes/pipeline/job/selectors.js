import _ from 'lodash';

import {getPipelineJobs} from "../selectors";

export function getSelectedJobId(state) {
    return state?.job?.selectedJobId;
}

export function getJob(state) {
    const pipelineJobs = getPipelineJobs(state);
    const selectedJobId = getSelectedJobId(state);

    return _.find(pipelineJobs, {_id: selectedJobId});
}
