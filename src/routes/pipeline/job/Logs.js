import React, {Component} from "react";
import {connect} from "react-redux";
import Ansi from "ansi-to-react";
import {Paper} from "@material-ui/core";
import ChatTwoToneIcon from "@material-ui/icons/ChatTwoTone";
import PublishIcon from '@material-ui/icons/Publish';
import GetAppIcon from '@material-ui/icons/GetApp';

import {getJob} from "./selectors";

function formatLogs(logs = []) {
    let content = [];

    for (const log of logs) {
        content.push(log.split('\r\n').map((i,key) => <div key={key}><Ansi>{i}</Ansi></div>));
    }

    return content;
}

class LogsComponent extends Component {
    scrollToTop () {
        this.el && this.el.scrollTo(0, 0);
    }

    scrollToBottom () {
        if (this.el) {
            this.el.scrollTop = this.el.scrollHeight;
        }
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        const {logs} = this.props;
        if (!logs) {
            return null;
        }

        return (
            <Paper elevation={3} style={{padding: 20, marginLeft: 20}}>
                <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                    <ChatTwoToneIcon fontSize={'large'} style={{marginRight: 20}}/>
                    <h2>Logs</h2>
                    <div style={{flex: 1}}/>
                    <PublishIcon fontSize={'large'} onClick={() => this.scrollToTop()} style={{marginRight: 10}}/>
                    <GetAppIcon fontSize={'large'} onClick={() => this.scrollToBottom()} style={{marginRight: 10}}/>
                </div>
                <div style={{overflow: 'scroll', maxHeight: '80vh'}} ref={el => { this.el = el }}>
                    {formatLogs(logs)}
                </div>
            </Paper>
        );
    };
}

export const Logs = connect(state => ({logs: getJob(state)?.logs}))(LogsComponent);
