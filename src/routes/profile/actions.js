import {get} from "api";

export function setMe(me) {
    localStorage.setItem('me', JSON.stringify(me));
}

export async function fetchMe() {
    const me = await get('/me');

    me && setMe(me);
}

export function getMe() {
    const me = localStorage.getItem('me');

    if (me) {
        return JSON.parse(me);
    }
}

export function getUserPermissions() {
    return getMe()?.permissions || [];
}

export function isAdmin() {
    return getUserPermissions().indexOf('admin') !== -1;
}