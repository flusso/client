import _ from "lodash";

export function getFirstJobNames(jobs) {
    let names = [];
    const allChildren = _.flatten(_.map(jobs, 'children'));

    for (const {name} of (jobs || [])) {
        if (!_.includes(allChildren, name)) {
            names.push(name);
        }
    }

    return names;
}
