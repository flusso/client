import {get} from 'api';

export function fetchPluginsFromRepo() {
    return async dispatch => {
        const pluginsFromRepo = await get(`/plugins`);

        return dispatch({type: 'SET_PLUGINS_FROM_REPO', pluginsFromRepo})
    }
}

export function fetchFlowPlugins(flowId) {
    return async dispatch => {
        const flowPlugins = await get(`/flows/${flowId}/plugins`);

        return dispatch({type: 'SET_FLOW_PLUGINS', flowPlugins})
    }
}
