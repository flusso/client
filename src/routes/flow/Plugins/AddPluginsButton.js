import React from 'react';
import {connect} from "react-redux";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import CircularProgress from "@material-ui/core/CircularProgress";
import {Button, Grid} from "@material-ui/core";

import {hideDialog, showDialog} from "components";

import {getPluginsFromRepo, getSingleJobpluginIds} from "./selectors";
import {updateJob} from "../actions";
import {addPlugin} from "../Plugin/actions";

function PluginListComponent({plugins, addPlugin}) {
    if (!plugins) {
        return null;
    }

    return (
        <List>
            {plugins.map(({name, _id}) => (
                <ListItem button key={name} onClick={() => addPlugin(name)}>
                    <ListItemText primary={name}/>
                </ListItem>
            ))}
        </List>
    );
}

const PluginList = connect(state => ({
    plugins: getPluginsFromRepo(state),
    pluginIds: getSingleJobpluginIds(state)
}), {addPlugin, updateJob, hideDialog})(PluginListComponent);

function AddPluginsButtonComponent({plugins, showDialog}) {
    return (
        <Grid item={true} xs={12}>
            <Button
                style={{marginTop: 20, marginBottom: 20}}
                size={'medium'}
                variant={"outlined"}
                color={"primary"}
                startIcon={!plugins && <CircularProgress size={14}/>}
                onClick={() => !plugins ? {} : showDialog({content: () => <PluginList/>})}
            >
                Add Plugin
            </Button>
        </Grid>
    );
}

export const AddPluginsButton = connect(state => ({plugins: getPluginsFromRepo(state)}), {showDialog})(AddPluginsButtonComponent);
