import {registerReducer} from 'store';

const plugins = {
    SET_PLUGINS_FROM_REPO(state, {pluginsFromRepo}) { return {...state, pluginsFromRepo} },
    SET_FLOW_PLUGINS(state, {flowPlugins}) { return {...state, flowPlugins} }
};

const singlePlugin = {
    SET_PLUGIN(state, {plugin}) { return {...state, plugin} }
}

registerReducer('singlePlugin', singlePlugin);
registerReducer('plugins', plugins);
