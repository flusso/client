import React from 'react';
import {connect} from "react-redux";
import {IconButton, Paper} from '@material-ui/core';
import DeleteForeverTwoToneIcon from '@material-ui/icons/DeleteForeverTwoTone';
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';

import {getSingleJobPlugins} from "./selectors";
import {editPlugin, removePlugin} from "../Plugin/actions";

function PluginComponent({plugin, editPlugin, removePlugin}) {
    return (
        <Paper elevation={2} xs={5} style={{marginBottom: 5, padding: 10}}>
            <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                <div style={{flexDirection: 'column'}}>
                    <h3>Plugin: {plugin.name}</h3>
                    <h3>Version: {plugin.version || 'Latest'}</h3>
                </div>
                <div>
                    <IconButton onClick={() => editPlugin(plugin._id)} variant={'contained'} color={'primary'}><EditTwoToneIcon/></IconButton>
                    {plugin._id && <IconButton onClick={() => removePlugin(plugin._id)} variant={'contained'} color={'secondary'}><DeleteForeverTwoToneIcon/></IconButton>}
                </div>
            </div>
        </Paper>
    );
}

function PluginsComponent({plugins}) {
    return (
        <div style={{marginTop: 10, marginBottom: 15}}>
            {plugins.map((plugin, idx) => <Plugin key={idx} plugin={plugin}/>)}
        </div>
    );
}

const Plugin = connect(null, {removePlugin, editPlugin})(PluginComponent);
export const Plugins = connect(state => ({plugins: getSingleJobPlugins(state)}))(PluginsComponent);
