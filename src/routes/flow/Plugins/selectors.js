import _ from 'lodash';

import {getSingleJob} from "../selectors";

export function getPluginsFromRepo(state) {
    return _.get(state, 'plugins.pluginsFromRepo');
}

export function getFlowPlugins(state) {
    return _.get(state, 'plugins.flowPlugins');
}

export function getSingleJobpluginIds(state) {
    const job = getSingleJob(state);

    return _.get(job, 'pluginIds');
}

export function getSingleJobPlugins(state) {
    const ids = getSingleJobpluginIds(state);

    return _.filter(getFlowPlugins(state), plugin => _.includes(ids, plugin._id));
}
