import _ from 'lodash';

export function getFlow(state) {
    return _.get(state, 'flow.flow');
}

export function getFlowDirtyness(state) {
    return !!_.get(state, 'flow.isDirty');
}

export function getSelectedJobName(state) {
    return _.get(state, 'singleJob.selectedJobName');
}

export function getSingleJob(state, jobName) {
    const flow = getFlow(state);

    return _.find(flow.jobs, {name: jobName || getSelectedJobName(state)});
}
