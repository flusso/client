import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {getFlow, getSelectedJobName} from './selectors';
import {setSelectedJobName, updateJob} from './actions';
import {getNodes, getNumberOfjobsPerLevel, handleClick} from "components/Chart/actions";
import {getFirstJobNames} from "./utils";
import {Chart} from "components/Chart/Chart";

class FlowChartComponent extends Component {
    componentDidMount() {
        const {flowJobs, selectedJobName, setSelectedJobName} = this.props;

        if (!selectedJobName || !_.find(flowJobs, {name: selectedJobName})) {
            const firstJob = _.first(getFirstJobNames(flowJobs));
            firstJob && setSelectedJobName(firstJob);
        }
    }

    render() {
        const {flowJobs, selectedJobName, setSelectedJobName, updateJob, handleClick} = this.props;
        let levels = getNumberOfjobsPerLevel({flowJobs, jobs: [], currentLevel: 0, levels: {}});
        levels.center = _.max(_.values(levels));
        const nodes = getNodes({
            totalJobs: flowJobs,
            level: 0,
            levels,
            selectedJob: selectedJobName,
            setSelectedJob: setSelectedJobName,
            handleClick,
            updateJob
        });

        return <Chart nodes={nodes}/>;
    }
}

export const FlowChart = connect(state => ({
    flowJobs: getFlow(state)?.jobs,
    selectedJobName: getSelectedJobName(state),
}), {setSelectedJobName, updateJob, handleClick})(FlowChartComponent);
