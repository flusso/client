import React from "react";
import {connect} from "react-redux";
import Grid from "@material-ui/core/Grid";
import {Button} from "@material-ui/core";
import SaveTwoToneIcon from "@material-ui/icons/Save";
import AddToPhotosTwoToneIcon from '@material-ui/icons/AddToPhotosTwoTone';
import LowPriorityIcon from '@material-ui/icons/LowPriority';
import Paper from "@material-ui/core/Paper";

import {addJob, reorderFlow, saveFlow} from "./actions";
import {getFlowDirtyness} from "./selectors";

function NewJobButtonComponent({addJob}) {
    return (
        <Grid style={{display: 'flex', flexDirection: 'column', marginBottom: 30}}>
            <Button
                variant={'contained'}
                startIcon={<AddToPhotosTwoToneIcon/>}
                color={'default'}
                onClick={addJob}
            >
                New Job
            </Button>
            <p style={{fontSize: 16}}>
                You can add and remove links between jobs selecting one of them, holding shift and clicking on the one you want to connect!
            </p>
        </Grid>
    );
}

const NewJobButton = connect(null, {addJob})(NewJobButtonComponent);

function ReorderButtonComponent({reorderFlow}) {
    return (
        <Grid style={{display: 'flex', flexDirection: 'column', marginBottom: 30}}>
            <Button
                variant={'contained'}
                startIcon={<LowPriorityIcon/>}
                color={'default'}
                onClick={reorderFlow}
            >
                Reorder Flow
            </Button>
            <p style={{fontSize: 16}}>
                Click this button to automatically reorder the jobs following a top-down schema!
            </p>
        </Grid>
    );
}

const ReorderButton = connect(null, {reorderFlow})(ReorderButtonComponent);

function EditorComponent({isFlowDirty, saveFlow}) {
    return (
        <Grid item={true} xs={2}>
            <Paper elevation={3} style={{justifyContent: 'center', display: 'flex', flexDirection: 'column', padding: 20}}>
                <NewJobButton/>
                <ReorderButton/>
                <Button disabled={!isFlowDirty} onClick={saveFlow} variant={'contained'} color={'primary'} startIcon={<SaveTwoToneIcon/>}>Save Flow</Button>
            </Paper>
        </Grid>
    )
}

export const Editor = connect(state => ({isFlowDirty: getFlowDirtyness(state)}), {saveFlow})(EditorComponent);
