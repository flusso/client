import {registerReducer} from 'store';

const flow = {
    SET_FLOW(state, {flow, isDirty}) { return {...state, flow, isDirty} },
};

const singleJob = {
    SET_SELECTED_JOB_NAME(state, {selectedJobName}) { return {...state, selectedJobName}; }
};

registerReducer('flow', flow);
registerReducer('singleJob', singleJob);
