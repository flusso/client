import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import DeleteForeverTwoToneIcon from "@material-ui/icons/DeleteForeverTwoTone";
import {Paper, Grid, TextareaAutosize, TextField, Button, Radio, FormControlLabel, RadioGroup, FormControl} from '@material-ui/core';

import {getFlow, getSelectedJobName, getSingleJob} from './selectors';
import {removeJob, updateJob} from "./actions";
import {AddPluginsButton, Plugins} from "./Plugins";

function EditableEntryComponent({text, value, property, updateJob}) {
    return (
        <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
            <h2>{text} </h2>
            <div style={{flex: 1}}/>
            <TextField
                variant={'outlined'}
                size={'small'}
                onChange={e => updateJob(property, e.target.value)}
                value={value}
                style={{marginLeft: 20}}
            />
        </div>
    );
}

function CodeEditorComponent({jobCode, updateJob}) {
    return (
        <>
            <TextareaAutosize rowsMin={5} value={jobCode} onChange={code => updateJob('scripts', code.target.value)}/>
            <Buttons/>
        </>
    );
}

function ButtonsComponent({removeJob}) {
    return (
        <div style={{display: 'flex', flex: 1, marginTop: 10, marginBottom: 10}}>
            <Button onClick={removeJob} variant={'contained'} color={'secondary'} startIcon={<DeleteForeverTwoToneIcon/>}>Delete Job</Button>
            <div style={{flex: 1}}/>
        </div>
    )
}

function ExecutionMethodsComponent({execution, updateJob}) {
    return (
        <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
            <h2>Execution </h2>
            <div style={{flex: 1}}/>
            <FormControl component="fieldset">
                <RadioGroup aria-label="gender" name="gender1" value={execution || 'automatic'} onChange={event => updateJob('execution', event.target.value)}>
                    <FormControlLabel value="automatic" control={<Radio />} label="Automatic" />
                    <FormControlLabel value="manual" control={<Radio />} label="Manual" />
                </RadioGroup>
            </FormControl>
        </div>
    );
}

function ShellComponent({shell, updateJob}) {
    return (
        <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
            <h2>Shell </h2>
            <div style={{flex: 1}}/>
            <FormControl component="fieldset">
                <RadioGroup aria-label="gender" name="gender1" value={shell || 'bash'} onChange={event => updateJob('shell', event.target.value)}>
                    <FormControlLabel value="bash" control={<Radio />} label="Bash" />
                    <FormControlLabel value="sh" control={<Radio />} label="Sh" />
                </RadioGroup>
            </FormControl>
        </div>
    );
}

export function SingleJobComponent({flow, selectedJobName}) {
    const job = _.find(flow.jobs, {name: selectedJobName});
    if (_.isEmpty(job)) { return null; }

    const {tags = [], image, name} = job;

    return (
        <Grid item={true} xs={4}>
            <Paper elevation={3} style={{marginBottom: 10, padding: 10, justifyContent: 'center', display: 'flex', flexDirection: 'column'}}>
                <EditableEntry text={'Job name'} value={name} property={'name'}/>
                <EditableEntry text={'Tags (comma separated)'} value={tags.join(', ')} property={'tags'}/>
                <EditableEntry text={'Docker image'} value={image} property={'image'}/>
                <Shell/>
                <ExecutionMethods/>
                <AddPluginsButton/>
                <Plugins/>
                <CodeEditor/>
            </Paper>
        </Grid>
    );
}

const ExecutionMethods = connect(state => ({execution: getSingleJob(state).execution}), {updateJob})(ExecutionMethodsComponent);
const Shell = connect(state => ({shell: getSingleJob(state).shell}), {updateJob})(ShellComponent);
const Buttons = connect(null, {removeJob})(ButtonsComponent);
const CodeEditor = connect(state => ({jobCode: getSingleJob(state).scripts}), {updateJob})(CodeEditorComponent);
const EditableEntry = connect(null, {updateJob})(EditableEntryComponent);
export const SingleJob = connect(state => ({selectedJobName: getSelectedJobName(state), flow: getFlow(state)}))(SingleJobComponent);
