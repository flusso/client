import React, {Component} from 'react';
import {connect} from "react-redux";
import _ from 'lodash';
import {Button, Grid, TextField} from "@material-ui/core";
import DeleteForeverTwoToneIcon from "@material-ui/icons/DeleteForeverTwoTone";
import PlayCircleFilledTwoToneIcon from "@material-ui/icons/PlayCircleFilledTwoTone";

import {BackButton} from "components";
import {registerRoute} from "router";

import {getFlow} from "./selectors";
import {fetchFlow, setFlow, showDeleteFlowDialog, updateFlow} from "./actions";
import {SingleJob} from "./SingleJob";
import {Editor} from "./Editor";
import {FlowChart} from "./FlowChart";
import {createPipeline} from "../flows/actions";

function TopBarComponent({flow, createPipeline, updateFlow, showDeleteFlowDialog}) {
    return (
        <Grid item={true} xs={12}>
            <span style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}>
                <h2>Flow name</h2>
                <TextField
                    variant={'outlined'}
                    onChange={e => updateFlow('name', e.target.value)}
                    value={flow.name}
                    style={{marginLeft: 20}}
                />
                <div style={{flex: 1}}/>
                <Button onClick={() => createPipeline(flow._id)} variant={'contained'} color={'primary'} startIcon={<PlayCircleFilledTwoToneIcon/>}>Trigger New Pipeline</Button>
                <Button onClick={showDeleteFlowDialog} variant={'contained'} style={{marginLeft: 20}} color={'secondary'} startIcon={<DeleteForeverTwoToneIcon/>}>Delete Flow</Button>
            </span>
        </Grid>
    );
}


class FlowComponent extends Component {
    componentDidMount() {
        const {fetchFlow, match} = this.props;
        fetchFlow(match.params.flowId);
    }

    componentWillUnmount() {
        this.props.setFlow({}, false);
    }

    render() {
        const {flow} = this.props;
        if (_.isEmpty(flow)) {
            return null;
        }

        return (
            <div style={{flex: 1}}>
                <BackButton/>
                <Grid>
                    <Grid container={true} spacing={2}>
                        <TopBar/>
                    </Grid>
                    <Grid container={true} spacing={2}>
                        <Editor/>
                        <Grid item={true} xs={6}>
                            <FlowChart/>
                        </Grid>
                        <SingleJob/>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const Flow = connect(state => ({flow: getFlow(state)}), {fetchFlow, setFlow})(FlowComponent);
const TopBar = connect(state => ({flow: getFlow(state)}), {updateFlow, createPipeline, showDeleteFlowDialog})(TopBarComponent);
registerRoute({path: '/flows/:flowId', component: Flow});
