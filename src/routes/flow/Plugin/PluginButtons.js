import React from 'react';
import {connect} from "react-redux";
import Button from "@material-ui/core/Button";

import {hideDialog} from "components";

import {savePlugin} from "./actions";

function SavePluginComponent({savePlugin, edit = false}) {
    return <Button onClick={() => savePlugin(edit)} color={"primary"}>Save Plugin</Button>
}

export const SavePlugin = connect(null, {savePlugin})(SavePluginComponent);

function CloseButtonComponent({hideDialog}) {
    return <Button onClick={hideDialog} color={"secondary"}>Close</Button>;
}

export const CloseButton = connect(null, {hideDialog})(CloseButtonComponent);
