import React from 'react';
import {connect} from "react-redux";
import _ from 'lodash';

import {getPlugin} from "./selectors";
import Grid from "@material-ui/core/Grid";
import {TextField} from "@material-ui/core";
import {updatePlugin} from "./actions";

function InputComponent({config, plugin, updatePlugin}) {
    const currentValue = _.get(plugin, `variables.${config.variable}`, config.value);

    return (
        <TextField
            variant={'outlined'}
            size={'small'}
            type={config.type}
            onChange={e => updatePlugin(`variables.${config.variable}`, e)}
            value={currentValue || ''}
        />
    );
}

const Input = connect(state => ({plugin: getPlugin(state)}), {updatePlugin})(InputComponent);

function SingleConfig({config}) {
    return (
        <div>
            <h3>{config.title}</h3>
            <h4>{config.description}</h4>
            {!!config.type && <Input config={config}/>}
        </div>
    );
}

function PluginComponent({plugin}) {
    return (
        <Grid>
            <h2>{plugin.title}</h2>
            {(plugin.config || []).map(config => <SingleConfig config={config}/>)}
        </Grid>
    );
}

export const Plugin = connect(state => ({plugin: getPlugin(state)}))(PluginComponent);
