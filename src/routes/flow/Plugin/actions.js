import React from 'react';
import _ from "lodash";

import {hideDialog, showDialog} from "components";
import {get, post, put} from "api";

import {getSingleJobpluginIds, getSingleJobPlugins} from "../Plugins/selectors";
import {getPlugin} from "./selectors";
import {Plugin} from "./Plugin";
import {CloseButton, SavePlugin} from "./PluginButtons";
import {saveFlow, updateJob} from "../actions";

function showPluginDialog(edit) {
    return async (dispatch, getState) => {
        const plugin = getPlugin(getState());

        dispatch(showDialog({
            title: `Edit plugin: ${plugin.name}`,
            content: () => <Plugin/>,
            buttons: [<CloseButton/>, <SavePlugin edit={edit}/>]
        }));
    };
}

function setSinglePlugin(plugin) {
    return {type: 'SET_PLUGIN', plugin};
}

export function addPlugin(name) {
    return async dispatch => {
        const plugin = await get(`/pluginFromRepo/${name}`);

        dispatch(hideDialog());
        dispatch(setSinglePlugin(plugin));
        dispatch(showPluginDialog());
    }
}

export function updatePlugin(property, event) {
    return (dispatch, getState) => {
        const state = getState();
        const value = _.get(event, ['target', 'value'], event);
        const plugin = getPlugin(state);

        _.set(plugin, property, value);

        dispatch(setSinglePlugin({...plugin}));
    };
}

export function savePlugin(edit) {
    return async(dispatch, getState) => {
        const state = getState();
        const plugin = getPlugin(state);

        if (plugin) {
            if (edit) {
                await put(`/plugins/${plugin._id}`, plugin);
            } else {
                const newPlugin = await post('/plugins', plugin);

                const pluginIds = getSingleJobpluginIds(state);
                dispatch(updateJob('pluginIds', [...(pluginIds || []), newPlugin._id]));
                await dispatch(saveFlow());
            }

            dispatch(hideDialog());
        }
    };
}

export function removePlugin(pluginId) {
    return (dispatch, getState) => {
        const jobPlugins = getSingleJobPlugins(getState());

        if (_.find(jobPlugins, {_id: pluginId})) {
            _.remove(jobPlugins, {_id: pluginId});
            dispatch(updateJob('pluginIds', _.map(jobPlugins,  '_id')));
        }
    }
}

export function editPlugin(pluginId) {
    return (dispatch, getState) => {
        const jobPlugins = getSingleJobPlugins(getState());
        const plugin = _.find(jobPlugins, {_id: pluginId});

        if (plugin) {
            dispatch(setSinglePlugin(plugin));
            dispatch(showPluginDialog(true));
        }
    }
}
