import _ from "lodash";

export function getPlugin(state) {
    return _.get(state, 'singlePlugin.plugin');
}
