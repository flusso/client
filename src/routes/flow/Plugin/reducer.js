import {registerReducer} from 'store';

const plugin = {
    SET_PLUGIN(state, {plugin}) { return {...state, plugin} }
}

registerReducer('plugin', plugin);
