import _ from 'lodash';
import React from "react";
import {push} from 'connected-react-router'
import Button from "@material-ui/core/Button";

import {del, get, post, put} from 'api';
import {hideDialog, showDialog} from "components";

import {getFlow, getSelectedJobName, getSingleJob} from "./selectors";
import {fetchFlowPlugins, fetchPluginsFromRepo} from "./Plugins/actions";
import {getFirstJobNames} from "./utils";

export function setFlow(flow, isDirty = true) {
    return {type: 'SET_FLOW', flow, isDirty};
}

export function setSelectedJobName(selectedJobName) {
    return {type: 'SET_SELECTED_JOB_NAME', selectedJobName};
}

function getNewName(flow) {
    let currentLength = flow.jobs.length;

    while (true) {
        const newName = `New job ${currentLength}`;
        const existingJob = _.find(flow.jobs, {name: newName});

        if (!existingJob) {
            return newName;
        } else {
            currentLength++;
        }
    }
}

export function addJob() {
    return (dispatch, getState) => {
        const flow = getFlow(getState());
        const newName = getNewName(flow);

        const baseJob = {
            tags: [],
            children: [],
            name: newName,
            image: '',
            scripts: '',
            chart: {x: 30, y: 30}
        };

        flow.jobs.push(baseJob);
        dispatch(setFlow({...flow}));
        dispatch(setSelectedJobName(newName));
    };
}

function updateChildrenName(flow, currentName, value) {
    for (const jobIndex in flow.jobs) {
        if (_.includes(flow.jobs[jobIndex].children, currentName)) {
            _.pull(flow.jobs[jobIndex].children, currentName);
            flow.jobs[jobIndex].children.push(value);
        }
    }

    return flow;
}

export function updateFlow(property, event) {
    return (dispatch, getState) => {
        const state = getState();
        const value = _.get(event, ['target', 'value'], event);
        let flow = getFlow(state);

        _.set(flow, property, value);

        dispatch(setFlow({...flow}));
    }
}

export function updateJob(property, event, jobName) {
    return (dispatch, getState) => {
        const state = getState();
        let value = _.get(event, ['target', 'value'], event);
        let flow = getFlow(state);
        const jobIndex = _.findIndex(flow.jobs, {name: jobName || getSelectedJobName(state)});
        const currentName = flow.jobs[jobIndex].name;
        let isValidChange = true;

        if (property === 'name') {
            isValidChange = false;
            const isAlreadyExisting = !!_.size(_.filter(flow.jobs, {name: value}));

            if (!isAlreadyExisting) {
                dispatch(setSelectedJobName(value));
                flow = updateChildrenName(flow, currentName, value);
                isValidChange = true;
            }
        }

        if (property === 'tags') {
            if (value.indexOf(',') !== -1) {
                value = (value || '').split(', ');
            } else {
                value = [value];
            }
            value = value.map(el => el.trim())
        }

        if (isValidChange) {
            _.set(flow.jobs[jobIndex], property, value);
        }

        dispatch(setFlow({...flow}));
    };
}

export function saveFlow() {
    return async (dispatch, getState) => {
        try {
            const state = getState();
            let flow = getFlow(state);
            let flowId = flow._id;

            if (flowId) {
                await put(`/flows/${flowId}`, flow);
            } else {
                const newFlow = await post('/flows', flow);
                flowId = newFlow._id;
            }

            dispatch(fetchFlow(flowId))
            dispatch(push(`/flows/${flowId}`));
        } catch (e) {
            console.log(e);
        }
    }
}

export function reorderFlow() {
    return async (dispatch, getState) => {
        let flow = getFlow(getState());

        for (const job in flow.jobs) {
            delete flow.jobs[job].chart;
        }

        dispatch(setFlow({...flow}));
    }
}

export function removeJob() {
    return async (dispatch, getState) => {
        const state = getState();
        let flow = getFlow(state);
        const jobToBeRemoved = getSingleJob(state);
        const fathersOfThisJob = _.filter(flow.jobs, job => _.includes(job.children, jobToBeRemoved.name));

        for (const father of fathersOfThisJob) {
            const fatherIndex = _.findIndex(flow.jobs, {name: father.name});

            _.pull(flow.jobs[fatherIndex].children, jobToBeRemoved.name);
        }

        const newJobs = _.filter(flow.jobs, ({name}) => name !== jobToBeRemoved.name);

        _.set(flow, 'jobs', newJobs);

        dispatch(setSelectedJobName(''));
        dispatch(setFlow({...flow}));
    }
}

export function fetchFlow(flowId) {
    return async (dispatch, getState) => {
        let flow;
        if (flowId === 'new') {
            flow = {
                name: 'New flow',
                active: true,
                jobs: [{name: 'New job', children: [], chart: {x: 30, y: 30}}]
            };
        } else {
            flow = await get(`/flows/${flowId}`);
            dispatch(fetchFlowPlugins(flowId));
        }

        dispatch(fetchPluginsFromRepo());
        dispatch(setFlow(flow, false));

        if (!getSelectedJobName(getState())) {
            dispatch(setSelectedJobName(_.first(getFirstJobNames(flow.jobs))));
        }
    }
}

export function showDeleteFlowDialog() {
    return async (dispatch, getState) => {
        dispatch(showDialog({
            title: 'Are you sure you want to delete this flow?',
            content: () => <h3>This action will delete the flow and all the related pipelines!</h3>,
            buttons: [
                <Button onClick={() => dispatch(hideDialog())} color={"primary"}>Close</Button>,
                <Button onClick={async () => {
                    const state = getState();
                    const {_id} = getFlow(state);

                    await del(`/flows/${_id}`);

                    dispatch(push('/flows'));
                    dispatch(hideDialog());
                }}
                color={"secondary"}>
                    Delete
                </Button>
            ]
        }));
    }
}