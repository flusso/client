import _ from 'lodash';
import {push} from 'connected-react-router'

import {post} from 'api';
import {setTokenInApp} from 'auth';

import {fetchMe} from "../profile";

export function setTokenInStore(token) {
    return {type: 'SET_TOKEN', token};
}

export function login(account, password) {
    return async dispatch => {
        const token = await post('/login', {account, password});

        if (!_.isEmpty(token)) {
            setTokenInApp(token);
            dispatch(setTokenInStore(token));

            await fetchMe();
            dispatch(push('/flows'));
        }
    }
}
