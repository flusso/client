import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Paper} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

import {registerRoute} from "router";
import {Link} from "components";

import {login} from "./actions";

class LoginComponent extends Component {
    state = {account: '', password: ''};

    render() {
        const {login} = this.props;

        const {account, password} = this.state;

        return (
            <Grid item={true} xs={12} style={{justifyContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column'}}>
                <Paper elevation={5} style={{padding: 50, flexDirection: 'column', display: 'flex', zIndex: 1}}>
                    <h1 style={{textAlign: 'center'}}>Login</h1>
                    <TextField margin={'normal'} id="account" label="Email / Username" variant="outlined" value={account} onChange={e => this.setState({account: e.target.value})}/>
                    <TextField type={'password'} margin={'normal'} id="password" label="Password" variant="outlined" value={password} onChange={e => this.setState({password: e.target.value})}/>
                    <Button
                        type={'submit'}
                        style={{marginTop: 40}}
                        variant={'outlined'}
                        size={'large'}
                        color={'primary'}
                        onClick={() => login(account, password)}
                    >
                        Submit
                    </Button>
                </Paper>
                <Paper elevation={3} style={{padding: '0 15px', flexDirection: 'column', display: 'flex'}}>
                    <p>Not registered yet? <Link to={'/signup'}>Let's fix it!</Link></p>
                </Paper>
            </Grid>
        );
    }
}

const Login = connect(null, {login})(LoginComponent);
registerRoute({path: '/login', component: Login, auth: false});
