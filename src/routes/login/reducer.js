import {registerReducer} from 'store';

const token = {
    SET_TOKEN(state, {token}) { return token; },
    initialState: {}
};

registerReducer('token', token);
