import React, {Component} from 'react';
import {connect, Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router'
import {createBrowserHistory} from 'history'
import {MuiThemeProvider, Grid, CssBaseline} from '@material-ui/core';

import {theme, Menu, Dialog, Message} from 'components';
import {getStore} from 'store';
import {getAuthRouteComponents, getPublicRouteComponents} from 'router';
import {loadTokenInApp} from 'auth';
import {configureStore} from "api";

import './routes';

const history = createBrowserHistory();
const store = getStore(history);

configureStore(store);

class FlussoComponent extends Component {
    componentDidMount() {
        this.props.loadTokenInApp();
    }

    render() {
        return (
            <Grid container={true} style={{height: '100vh', padding: 50}}>
                <Menu/>
                {getPublicRouteComponents()}
                {getAuthRouteComponents()}
            </Grid>
        );
    }
}

const Flusso = connect(null, {loadTokenInApp})(FlussoComponent);

function App() {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <MuiThemeProvider theme={theme}>
                    <CssBaseline/>
                    <Dialog/>
                    <Message/>
                    <Flusso/>
                </MuiThemeProvider>
            </ConnectedRouter>
        </Provider>
    );
}

export default App;
